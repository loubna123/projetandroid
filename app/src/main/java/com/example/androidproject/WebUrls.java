package com.example.androidproject;

import android.net.Uri;
import java.net.MalformedURLException;
import java.net.URL;

public class WebUrls {

    private static final String BaseHost = "demo-lia.univ-avignon.fr";
    private static final String SubHost = "cerimuseum";
    private static final String SEARCH_CATALOG = "collection";
    private static final String SEARCH_CATEGORIES = "categories";
    private static final String SEARCH_ITEMS = "items";
    private static final String SEARCH_THUMBNAIL = "thumbnail";

    private static Uri.Builder commonBuilder() {
        Uri.Builder builder = new Uri.Builder();

        builder.scheme("https")
                .authority(BaseHost)
                .appendPath(SubHost);
        return builder;
    }

    public static URL buildSearchCategories() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_CATEGORIES);
        URL url = new URL(builder.build().toString());
        return url;
    }

    public static URL buildSearchCatalog() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_CATALOG);
        URL url = new URL(builder.build().toString());
        return url;
    }

    public static URL buildGetThumbnailById(String idAtefact) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_ITEMS).appendPath(idAtefact).appendPath(SEARCH_THUMBNAIL);
        URL url = new URL(builder.build().toString());
        return url;
    }

}
