package com.example.androidproject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import com.example.androidproject.R;

public class SplashScreenActivity extends AppCompatActivity {
    private long ms=0, splashtime=3000;
    private boolean splashActivityRun=true, paused=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        final ConstraintLayout cl=findViewById(R.id.splachlayout);
        Thread thread=new Thread(){
          public void run(){
              try{
                while(splashActivityRun && ms<splashtime){
                    if(!paused){//Condition d'arret
                        ms=ms+150;
                    }
                    sleep(150);
                }
              }catch (Exception ignored){
              }finally {
                  if(!isOnline()){
                      Snackbar snackbar=Snackbar
                              .make(cl,"Problem du Connexion",Snackbar.LENGTH_INDEFINITE)
                              .setAction("Réessayer", v -> recreate());
                      snackbar.show();
                  }else {
                      startMainActivity();
                  }
              }
          }
        };
        thread.start();
    }

    private boolean isOnline(){
        ConnectivityManager connectivityManager= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnectedOrConnecting();
    }
    private void startMainActivity() {
        Intent intent=new Intent(SplashScreenActivity.this,MainActivity.class);
        startActivity(intent);
        finish();
    }
}
