package com.example.androidproject;

import android.content.Context;
import android.os.AsyncTask;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import com.example.androidproject.ArtAdapter;
import com.example.androidproject.JSONResponseHandlerCategories;

public class CategoriesActivity extends AsyncTask<URL,Integer, ArrayList<Section>> {
    private static final String TAG = CategoriesActivity.class.getSimpleName();

    private List<ArtAdapter> artAdapterList;
    private List<Section> sectionList;
    private Context context;

    public CategoriesActivity(List<ArtAdapter> artAdapterList, List<Section> sectionList, Context context) {
        this.artAdapterList = artAdapterList;
        this.sectionList = sectionList;
        this.context = context;
    }

    @Override
    protected ArrayList<Section> doInBackground(URL... urls) {
        InputStream inputStream;
        JSONResponseHandlerCategories jsonResponseHandlerCategories=new JSONResponseHandlerCategories(this.sectionList);
        HttpURLConnection httpURLConnection;
        try {
            httpURLConnection=(HttpURLConnection) urls[0].openConnection();
            inputStream=new BufferedInputStream(httpURLConnection.getInputStream());
            try{
                jsonResponseHandlerCategories.readJsonStream(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return (ArrayList<Section>) this.sectionList;
    }

    @Override
    protected void onPostExecute(ArrayList<Section> sections) {
        super.onPostExecute(sections);
        for (int i = 0; i < sectionList.size(); i++) {
            sectionList.get(i).populateListSection(artAdapterList);
        }
        Collator COLLATOR = Collator.getInstance(Locale.FRENCH);
        Collections.sort(sectionList, (o1, o2) -> COLLATOR.compare(o1.getSectionName(), o2.getSectionName()));
    }
}
