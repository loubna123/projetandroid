package com.example.androidproject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import java.util.List;
import com.example.androidproject.ArtSectionAdapter;
import com.example.androidproject.R;

public class SectionActivity extends AppCompatActivity {
    private ArtSectionAdapter artSectionAdapter;
    private RecyclerView recyclerViewSection;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_section);

        List<Section> sectionList = MainActivity.sectionList;
        recyclerViewSection = findViewById(R.id.recyclerViewSection);
        artSectionAdapter = new ArtSectionAdapter(sectionList, this);
        recyclerViewSection.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerViewSection.setAdapter(artSectionAdapter);
        recyclerViewSection.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));


    }
}
