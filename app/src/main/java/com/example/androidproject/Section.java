package com.example.androidproject;

import java.util.ArrayList;
import java.util.List;
import com.example.androidproject.ArtAdapter;

public class Section {
    private String sectionName;
    private List<ArtAdapter> artAdapterListSection =new ArrayList<>();

    public Section(String sectionName) {
        this.sectionName = sectionName;
    }
    public String getSectionName() {
        return sectionName;
    }
    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }
    public List<ArtAdapter> getArtAdapterListSection() {
        return artAdapterListSection;
    }

    public void populateListSection(List<ArtAdapter> artAdapterListAll){
        for (int i = 0; i < artAdapterListAll.size(); i++) {
            for (int j = 0; j < artAdapterListAll.get(i).getCategories().size(); j++) {
                if(artAdapterListAll.get(i).getCategories().get(j).equals(sectionName)){
                    artAdapterListSection.add(artAdapterListAll.get(i));
                }
            }
        }
    }
    public ArtAdapter getArtPosition(int position){
        return artAdapterListSection.get(position);
    }
}
