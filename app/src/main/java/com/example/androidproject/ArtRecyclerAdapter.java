package com.example.androidproject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;
import com.example.androidproject.R;

public class ArtRecyclerAdapter extends RecyclerView.Adapter<ArtRecyclerAdapter.ArtefactViewHolder> implements Filterable {
    private List<ArtAdapter> artAdapterListAdapter;
    private List<ArtAdapter> artAdapterListAdapterAll;
    private Context context;
    private static ClickListener clickListener;

    public ArtRecyclerAdapter(Context context, List<ArtAdapter> artAdapterListAdapter) {
        this.context=context;
        this.artAdapterListAdapter = artAdapterListAdapter;
        this.artAdapterListAdapterAll = artAdapterListAdapter;
    }

    @NonNull
    @Override
    public ArtefactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_art, parent, false);
        return new ArtefactViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ArtefactViewHolder holder, int position) {
        holder.bind(artAdapterListAdapter.get(position));
    }

    @Override
    public int getItemCount() {
        return artAdapterListAdapter.size();
    }

    public ArtAdapter getArtefactAtPosition(int position){
        return artAdapterListAdapter.get(position);
    }
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charString = constraint.toString();
                if (charString.isEmpty()) {
                    artAdapterListAdapter = artAdapterListAdapterAll;
                } else {
                    List<ArtAdapter> filteredList = new ArrayList<>();
                    for (ArtAdapter artAdapter : artAdapterListAdapterAll) {

                        if (artAdapter.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(artAdapter);
                        }else {
                            for (int i = 0; i < artAdapter.getCategories().size(); i++) {
                                if(artAdapter.getCategories().get(i).contains(charString.toLowerCase())){
                                    filteredList.add(artAdapter);
                                }
                            }
                        }
                    }
                    artAdapterListAdapter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = artAdapterListAdapter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                    artAdapterListAdapter = (List<ArtAdapter>) results.values;
                    notifyDataSetChanged();
            }
        };
    }

    public interface ClickListener{
        void onItemClick(int position,View v);
    }
    public void setOnItemClicklistener(ClickListener clicklistener){
        clickListener=clicklistener;
    }

    static class ArtefactViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView artefactNameHolder;
        private TextView artefactBrandHolder;
        private ImageView artefactImageHolder;
        private TextView artefactCategoryHolder;

        ArtefactViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            artefactNameHolder = itemView.findViewById(R.id.artNameItem);
            artefactBrandHolder = itemView.findViewById(R.id.artBrandItem);
            artefactImageHolder = itemView.findViewById(R.id.artImageItem);
            artefactCategoryHolder = itemView.findViewById(R.id.artCategorieItem);
        }

        void bind(ArtAdapter item) {
            artefactNameHolder.setText(item.getName());
            artefactBrandHolder.setText(item.getBrand());
            artefactCategoryHolder.setText(item.getCategoriesToString());
            if (item.getThumbnail() != null && !item.getThumbnail().isEmpty()) {
                Picasso.with(itemView.getContext()).load(item.getThumbnail()).into(artefactImageHolder);
            } else {
                artefactImageHolder.setImageResource(R.mipmap.ic_launcher);
            }
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(),v);
        }
    }
}
