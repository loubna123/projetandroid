package com.example.androidproject;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;
import androidx.annotation.RequiresApi;

public class ArtAdapter implements Parcelable {

    public static final String TAG = ArtAdapter.class.getSimpleName() ;
    private String id;
    private String name;
    private List<String> categories=new ArrayList<>();;
    private String description;
    private List<Integer> timeFrame=new ArrayList<>();;
    private int year;
    private String brand;
    private String[] technicalDetails;
    private String[] pictures;
    private String thumbnail;

    public ArtAdapter(String id) {
        this.id = id;
        this.year=-1;
    }

    public ArtAdapter(String id, String name, List<String> categories, String description, List<Integer> timeFrame) {
        this.id = id;
        this.name = name;
        this.categories = categories;
        this.description = description;
        this.timeFrame = timeFrame;
        this.year=-1;
    }

    public static final Creator<ArtAdapter> CREATOR = new Creator<ArtAdapter>() {
        @RequiresApi(api = Build.VERSION_CODES.Q)
        @Override
        public ArtAdapter createFromParcel(Parcel in) {
            return new ArtAdapter(in);
        }

        @Override
        public ArtAdapter[] newArray(int size) {
            return new ArtAdapter[size];
        }
    };

    public String getId() { return id; }
    public String getName() { return name; }

    public List<String> getCategories() {
        return categories;
    }
    public String getCategoriesToString() {
        if(categories!=null){
            StringBuilder result=new StringBuilder();
            for (String category : categories) {
                result.append(category+"/");
            }
            result.delete(result.length()-1,result.length());
            return result.toString();
        }
        return null;
    }
    public String getDescription() { return description; }
    public List<Integer> getTimeFrame() { return timeFrame; }
    public int getYear() { return year; }
    public String getBrand() { return brand; }
    public String[] getTechnicalDetails() { return technicalDetails; }
    public String[] getPictures() { return pictures; }
    public String getThumbnail() { return thumbnail; }

    public void setId(String id) { this.id = id; }
    public void setName(String name) { this.name = name; }
    public void setCategories(List<String> categories) { this.categories = categories; }
    public void setDescription(String description) { this.description = description; }
    public void setTimeFrame(List<Integer> timeFrame) { this.timeFrame = timeFrame; }
    public void setYear(int year) { this.year = year; }
    public void setBrand(String brand) { this.brand = brand; }
    public void setTechnicalDetails(String[] technicalDetails) { this.technicalDetails = technicalDetails; }
    public void setPictures(String[] pictures) { this.pictures = pictures; }
    public void setThumbnail(String thumbnail) { this.thumbnail = thumbnail; }
    public String toString() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @SuppressLint("NewApi")
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeList(categories);
        dest.writeString(description);
        dest.writeList(timeFrame);
        dest.writeInt(year);
        dest.writeString(brand);
        dest.writeStringArray(technicalDetails);
        dest.writeStringArray(pictures);
        dest.writeString(thumbnail);
    }
    @RequiresApi(api = Build.VERSION_CODES.Q)
    private ArtAdapter(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        in.readList(this.categories,String.class.getClassLoader());
        this.description = in.readString();
        in.readList(this.timeFrame,Integer.class.getClassLoader());
        this.year = in.readInt();
        this.brand = in.readString();
       this.technicalDetails = (String[]) in.readArray(getClass().getClassLoader());
        this.pictures = (String[]) in.readArray(getClass().getClassLoader());
        this.thumbnail = in.readString();
    }
}
