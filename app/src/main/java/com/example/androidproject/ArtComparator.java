package com.example.androidproject;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;
import com.example.androidproject.ArtAdapter;

public class ArtComparator implements Comparator<ArtAdapter> {

    private CompareOption optionComparator;
    private static Collator COLLATOR = Collator.getInstance(Locale.FRENCH);

    public ArtComparator(CompareOption optionComparator) {
        this.optionComparator = optionComparator;
    }

    @Override
    public int compare(ArtAdapter o1, ArtAdapter o2) {
        if(optionComparator==CompareOption.NOM){
            return COLLATOR.compare(o1.getName(),o2.getName());
        }
        else{
            int result=o1.getYear()-o2.getYear();
            if(result==0){
                return COLLATOR.compare(o1.getName(),o2.getName());
            }
            else {
                return result;
            }
        }
    }
    public enum CompareOption{
        NOM,YEAR;
    }
}
