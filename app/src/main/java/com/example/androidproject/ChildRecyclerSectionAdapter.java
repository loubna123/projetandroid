package com.example.androidproject;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.squareup.picasso.Picasso;
import java.util.List;
import com.example.androidproject.R;

public class ChildRecyclerSectionAdapter extends RecyclerView.Adapter<ChildRecyclerSectionAdapter.ViewHolderChild> {
    private List<ArtAdapter> artAdapterList;
    private static ClickListenerChild clickListenerSection;

    public ChildRecyclerSectionAdapter(List<ArtAdapter> artAdapterList) {
        this.artAdapterList = artAdapterList;
    }

    @NonNull
    @Override
    public ViewHolderChild onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        View view=layoutInflater.inflate(R.layout.item_art,parent,false);
        return new ViewHolderChild(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderChild holder, int position) {
        holder.bind(artAdapterList.get(position));
    }

    @Override
    public int getItemCount() {
        return artAdapterList.size();
    }

    public ArtAdapter getArtefactAtPosition(int position){
        return artAdapterList.get(position);
    }

    public interface ClickListenerChild{
        void onItemClickChild(ArtAdapter artAdapter, View v);
    }
    void setClickListenerSection(ClickListenerChild clickListenerSection){
        ChildRecyclerSectionAdapter.clickListenerSection =clickListenerSection;
    }

    class ViewHolderChild extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView artNameHolder;
        private TextView artBrandHolder;
        private ImageView artImageHolder;
        private TextView artCategoryHolder;

        ViewHolderChild(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            artNameHolder = itemView.findViewById(R.id.artNameItem);
            artBrandHolder = itemView.findViewById(R.id.artBrandItem);
            artImageHolder = itemView.findViewById(R.id.artImageItem);
            artCategoryHolder = itemView.findViewById(R.id.artCategorieItem);
        }

        void bind(ArtAdapter item) {
            artNameHolder.setText(item.getName());
            artBrandHolder.setText(item.getBrand());
            artCategoryHolder.setText(item.getCategoriesToString());
            if (item.getThumbnail() != null && !item.getThumbnail().isEmpty()) {
                Picasso.with(itemView.getContext()).load(item.getThumbnail()).into(artImageHolder);
            } else {
                artImageHolder.setImageResource(R.mipmap.ic_launcher);
            }
        }

        @Override
        public void onClick(View v) {
            clickListenerSection.onItemClickChild(getArtefactAtPosition(getAdapterPosition()),v);
        }
    }
}
