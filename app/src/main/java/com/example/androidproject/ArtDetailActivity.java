package com.example.androidproject;

import androidx.appcompat.app.AppCompatActivity;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.example.androidproject.R;
import com.example.androidproject.ArtAdapter;

public class ArtDetailActivity extends AppCompatActivity {
    private static final String TAG = ArtDetailActivity.class.getSimpleName();
    private ArtAdapter artAdapter;

    private TextView textArtName, textArtCategorie, textArtDescription, textArtBrand, textArtYear ;
    private ImageView imageArtThumbnail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_art_detail);
        artAdapter = getIntent().getParcelableExtra(ArtAdapter.TAG);
        textArtName=findViewById(R.id.artNameDetail);
        textArtCategorie=findViewById(R.id.artCategoriesDetail);
        textArtBrand=findViewById(R.id.brandView);
        textArtDescription=findViewById(R.id.descriptionView);
        textArtYear=findViewById(R.id.yearView);
        imageArtThumbnail=findViewById(R.id.artThumbnailDetail);
        updateView();
    }

    @SuppressLint("SetTextI18n")
    void updateView(){
        textArtName.setText(artAdapter.getName());
        textArtCategorie.setText(artAdapter.getCategoriesToString());
        if(artAdapter.getBrand()!=null){
            textArtBrand.setText(artAdapter.getBrand());
        }
        textArtDescription.setText(artAdapter.getDescription());
        if(artAdapter.getYear()!=-1){
            textArtYear.setText(Integer.toString(artAdapter.getYear()));
        }
        if (artAdapter.getThumbnail() != null && !artAdapter.getThumbnail().isEmpty()) {
            Picasso.with(this).load(artAdapter.getThumbnail()).into(imageArtThumbnail);
        } else {
            imageArtThumbnail.setImageResource(R.mipmap.ic_launcher);
        }
    }
}
