package com.example.androidproject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import com.example.androidproject.ArtAdapter;
import com.example.androidproject.JSONResponseHandlerArt;

@SuppressLint("StaticFieldLeak")
public class ArtActivity extends AsyncTask<URL,Integer, ArrayList<ArtAdapter>> {
    private List<ArtAdapter> artAdapterList;
    private Context context;

    public ArtActivity(List<ArtAdapter> artAdapterList, Context context) {
        this.artAdapterList = artAdapterList;
        this.context=context;
    }
    @Override
    protected ArrayList<ArtAdapter> doInBackground(URL... urls) {
        InputStream inputStream;
        JSONResponseHandlerArt jsonResponseHandlerArt =new JSONResponseHandlerArt(this.artAdapterList);
        HttpURLConnection httpURLConnection;

        try {
            httpURLConnection=(HttpURLConnection) urls[0].openConnection();
            inputStream=new BufferedInputStream(httpURLConnection.getInputStream());
            try{
                jsonResponseHandlerArt.readJsonStream(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return (ArrayList<ArtAdapter>) this.artAdapterList;
    }

    @Override
    protected void onPostExecute(ArrayList<ArtAdapter> artAdapters) {
        super.onPostExecute(artAdapters);
        if(context instanceof MainActivity){
            ((MainActivity) context).updateRecycler();
            ((MainActivity) context).swipeRefreshLayout.setRefreshing(false);
            Collections.sort(artAdapterList,new ArtComparator(ArtComparator.CompareOption.NOM));
        }
    }
}
