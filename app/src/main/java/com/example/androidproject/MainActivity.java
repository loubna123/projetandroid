package com.example.androidproject;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import com.example.androidproject.ArtAdapter;
import com.example.androidproject.ArtRecyclerAdapter;
import com.example.androidproject.R;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();
    public static List<ArtAdapter> artAdapterList = new ArrayList<>();
    public static List<Section> sectionList=new ArrayList<>();
    private RecyclerView recyclerView;
    private ArtRecyclerAdapter artRecyclerAdapter;
    public SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        swipeRefreshLayout= findViewById(R.id.swipeRefresh);
        recyclerView=findViewById(R.id.artefactListRecyclerView);

        if(artAdapterList.size()==0){
            ArtActivity artActivity =new ArtActivity(artAdapterList,MainActivity.this);
            try {
                artActivity.execute(WebUrls.buildSearchCatalog());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

        sectionList.clear();
        initSection();
        artRecyclerAdapter =new ArtRecyclerAdapter(this, artAdapterList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(artRecyclerAdapter);
        artRecyclerAdapter.setOnItemClicklistener((position, v) -> {
            Intent intent= new Intent(MainActivity.this, ArtDetailActivity.class);
            intent.putExtra(ArtAdapter.TAG, artRecyclerAdapter.getArtefactAtPosition(position));
            startActivity(intent);
        });

        swipeRefreshLayout.setOnRefreshListener(()->{
            if(artAdapterList.size()==0){
                initSection();
                ArtActivity artActivity =new ArtActivity(artAdapterList,MainActivity.this);
                try {
                    artActivity.execute(WebUrls.buildSearchCatalog());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

            }
            else{
                swipeRefreshLayout.setRefreshing(false);
            }
            sectionList.clear();
            initSection();
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem searchItem=menu.findItem(R.id.app_bar_search);
        SearchView searchView= (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                artRecyclerAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                artRecyclerAdapter.getFilter().filter(newText);
                return false;
            }
        });

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id==R.id.app_bar_search){
            return true;
        }
        if (id == R.id.action_settings) {
            Intent intent= new Intent(MainActivity.this,SectionActivity.class);
            startActivity(intent);
            return true;
        }
        else if(id==R.id.sortName){
            item.setChecked(!item.isChecked());
            Collections.sort(artAdapterList,new ArtComparator(ArtComparator.CompareOption.NOM));
            updateRecycler();
            return true;
        }
        else if(id==R.id.sortYear){
            item.setChecked(!item.isChecked());
            Collections.sort(artAdapterList,new ArtComparator(ArtComparator.CompareOption.YEAR));
            updateRecycler();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void updateRecycler(){
        recyclerView.setAdapter(artRecyclerAdapter);
    }
    public void initSection(){
        CategoriesActivity categoriesActivity =new CategoriesActivity(artAdapterList, sectionList, MainActivity.this);
        try {
            categoriesActivity.execute(WebUrls.buildSearchCategories());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
