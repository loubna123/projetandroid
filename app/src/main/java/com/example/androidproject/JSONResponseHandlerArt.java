package com.example.androidproject;

import android.util.JsonReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.example.androidproject.ArtAdapter;
import com.example.androidproject.WebUrls;

public class JSONResponseHandlerArt {
    private static final String TAG = JSONResponseHandlerArt.class.getSimpleName();
    private List<ArtAdapter> artAdapterList;

    public JSONResponseHandlerArt(List<ArtAdapter> artAdapterList) {
        this.artAdapterList = artAdapterList;
    }


    /**
     * @param response done by the Web service
     * @return A artefactList with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readCatalog(reader);
        } finally {
            reader.close();
        }

    }
    private void readCatalog(JsonReader reader) throws IOException {
        reader.beginObject();
        while(reader.hasNext()){
            ArtAdapter artAdapter =new ArtAdapter(reader.nextName());
            reader.beginObject();
            while (reader.hasNext()){
                String name=reader.nextName();
                switch (name){
                    case "name":
                        artAdapter.setName(reader.nextString());
                        break;
                    case "categories":
                        reader.beginArray();
                        List<String> tableauCategorie=new ArrayList<String>();
                        while (reader.hasNext()){
                            tableauCategorie.add(reader.nextString());
                        }
                        reader.endArray();
                        artAdapter.setCategories(tableauCategorie);
                        break;

                    case "description":
                        artAdapter.setDescription(reader.nextString());
                        break;
                    case "timeFrame":
                        reader.beginArray();
                        List<Integer> tableauTimeFrame=new ArrayList<Integer>();
                        while (reader.hasNext()){
                            tableauTimeFrame.add(reader.nextInt());
                        }
                        reader.endArray();
                        artAdapter.setTimeFrame(tableauTimeFrame);
                        break;

                    case "year":
                        artAdapter.setYear(reader.nextInt());
                        break;
                    case "brand":
                        artAdapter.setBrand(reader.nextString());
                        break;

                        case "technicalDetails":
                        reader.beginArray();
                        while (reader.hasNext()){
                            reader.skipValue();
                        }
                        reader.endArray();
                        break;

                    case "pictures":
                        reader.beginObject();
                        while (reader.hasNext()){
                            reader.skipValue();
                        }
                        reader.endObject();
                        break;
                    default:
                        reader.skipValue();
                        break;
                }
            }
            artAdapter.setThumbnail(WebUrls.buildGetThumbnailById(artAdapter.getId()).toString());
            artAdapterList.add(artAdapter);
            reader.endObject();
        }
    }
}
