package com.example.androidproject;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import com.example.androidproject.ArtDetailActivity;
import com.example.androidproject.Section;
import com.example.androidproject.R;

public class ArtSectionAdapter extends RecyclerView.Adapter<ArtSectionAdapter.ViewHolder> {
    private List<Section> sectionList;
    private Context context;
    public ArtSectionAdapter(List<Section> sectionList, Context context) {
        this.sectionList = sectionList;
        this.context=context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        View view=layoutInflater.inflate(R.layout.section_recycler,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(this.sectionList.get(position),context);
    }

    @Override
    public int getItemCount() {
        return sectionList.size();
    }

   static class ViewHolder extends RecyclerView.ViewHolder{
        private TextView sectionName;
        private RecyclerView childRecyclerView;
        private ChildRecyclerSectionAdapter childRecyclerSectionAdapter;

       public ViewHolder(@NonNull View itemView) {
            super(itemView);
            sectionName=itemView.findViewById(R.id.sectionName);
            childRecyclerView=itemView.findViewById(R.id.recyclerViewChild);
        }
        private void bind(Section section,Context context) {
            sectionName.setText(section.getSectionName());
            childRecyclerSectionAdapter=new ChildRecyclerSectionAdapter(section.getArtAdapterListSection());
            childRecyclerView.setAdapter(childRecyclerSectionAdapter);
            childRecyclerSectionAdapter.setClickListenerSection((artefact, v) -> {
                    Intent intent= new Intent(context, ArtDetailActivity.class);
                    intent.putExtra(ArtAdapter.TAG, artefact);
                    context.startActivity(intent);
            });
        }
    }
}
